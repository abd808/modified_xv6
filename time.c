#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"

int wtime, rtime, status;

int main(int argc, char **argv)
{
	int pid = fork();
	if (pid < 0)
	{
		printf(2, "Failed to fork\n");
		exit();
	}

	if (pid)
	{
		status = waitx(&wtime, &rtime);
		if (argc > 1)
			printf(1, "Time taken by %s\nWait time: %d\nRun time: %d with Status %d\n\n", argv[1], wtime, rtime, status);
			
		else if (argc == 1)
		{
			printf(1, "Wait time: %d\nRun time: %d with Status %d\n\n", wtime, rtime, status);
		}

		
		exit();
	}

	else if (!pid)
	{
		if(argc >1 )
		{
			printf(1, "Timing %s\n", argv[1]);
			if (exec(argv[1], argv+1) < 0)
			{
				printf(2, "Exec failed\n");
				exit();
			}
		}

		if (argc == 1)
		{
			volatile long long int x = 0, y=10000000;
			printf(1, "Default program being timed\n");
			while(y--)
			{
				x^=1;
			} // eating cpu cycles
			exit();
		}


	}

	
}